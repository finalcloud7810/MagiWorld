public class Rodeur extends Personnage {
    Rodeur(int joueur) {
        ajoutDesPointsDeCaracteristiques();
        setClasseJoueur("Chut je suis le Rôdeur",joueur);
    }
/**
 * Basic Attack : Damage = Character Agi
 * */
    @Override
    public void attaqueBasique(Personnage ennemi) {
        int tirAlArc = this.caracteristique[2];
        System.out.println("Joueur "+this.joueur+" utilise Tir à l'Arc et inflige "+tirAlArc+" dommages.");
        ennemi.caracteristique[4] -= tirAlArc;
        System.out.println("Le joueur"+ennemi.joueur+" perd "+tirAlArc+" point de vie");
        if (ennemi.caracteristique[4]<=0)
            System.out.println("Joueur " + ennemi.joueur + " est mort");

    }
/**
 * Special Attack : 1/2 Agi + Character Agi = new Character Agi
 * */
    @Override
    public void attaqueSpecial(Personnage ennemi) {
        int concentration = this.caracteristique[0] / 2;
        System.out.println("Joueur "+this.joueur+" utilise concentration et gagne "+concentration+" en agilité.");
        this.caracteristique[2] += concentration;
    }
}
