import java.util.Scanner;

public abstract class Personnage {
    private String classe;
    int joueur;
    int[] caracteristique = new int[5];
    /*
     * caracteristique[0] = niveau
     * caracteristique[1] = force
     * caracteristique[2] = agilité
     * caracteristique[3] = intelligence
     * caracteristique[4] = vie
     */
    private Scanner sc = new Scanner(System.in);

/**
 * Sentences for features choices
 * */
    private String texteCaracteristique(int index){
        String[] quelleCaracteristique = new String[]{
                "Niveau du personnage ?",
                "Force du personnage ?",
                "Agilité du personnage ?",
                "Intelligence du personnage ?"
        };
        return quelleCaracteristique[index];
    }
/**
 * Error messages for features and levels choice
 * */
    private boolean setCaracteristique(int index, int valeur){
        caracteristique[index] = valeur;
        caracteristique[4] = 5 * caracteristique[0];
        if (index == 0){
            if (valeur > 100){
                System.out.println("Votre niveau ne peut être supérieur à 100.");
                return false;
            }if (valeur < 1){
                System.out.println("Votre niveau ne peut être inférieur à 1.");
                return false;
            }
        }else{
            if (valeur > caracteristique[0]) {
                System.out.println("Vos caractéristiques ne peuvent pas être supérieures à votre niveau.");
                return false;
            }else if (valeur < 0) {
                System.out.println("Vos caractéristiques ne peuvent pas être négatives.");
                return false;
            }
        }
        return true;
    }
/**
 * check if feature point are egal to level player
 * if not display error message
 * */
    void ajoutDesPointsDeCaracteristiques(){
        for (int x=0;x<caracteristique.length-1;x++) {
            System.out.println(texteCaracteristique(x));
            int taCaracteristique = sc.nextInt();
            if (!setCaracteristique(x,taCaracteristique)){
                x--;
            }else if ((caracteristique[1] + caracteristique[2] + caracteristique[3] != caracteristique[0])&&(x == 3)) {
                System.out.println("Vos trois caractéristiques réunies ne sont pas égales au niveau de votre joueur.");
                for (int y = 1;y<caracteristique.length-1;y++)
                    caracteristique[y] = 0;
                x = 0;
            }
        }
    }
/**
 * display summary of character
 * */
    void setClasseJoueur(String classe, int joueur) {
        this.classe = classe;
        this.joueur = joueur;
        System.out.println(classe + " joueur " + joueur + ", niveau " + caracteristique[0] + ", je possède " + caracteristique[4] + " de vitalité, " + caracteristique[1] + " de force, " + caracteristique[2] +
                " d'agilité et " + caracteristique[3] + " d'intelligence !");

    }

/**
 * Basic attack for the fight
 * */
    public abstract void attaqueBasique(Personnage ennemi);

/**
 * Special Attack for the fight
 * */
    public abstract void attaqueSpecial(Personnage ennemi);
}
