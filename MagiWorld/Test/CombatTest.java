import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CombatTest {
    @Test
    void Given_Warrior_When_DisplayClassChoice_Then_DisplayWarriorClasse() {
        int choix = 1;
        switch (2){
            case 2: assertEquals(1,choix);
        }
    }
    @Test
    void Given_Alert_When_TurnStart_Then_DisplayTurnSentence(){
        int partieUne = 1,partieDeux = 2;
        for (int x=0;x<2;x++) {
            if(x==0) {
                partieUne++;
                partieDeux--;
            }
            else {
                partieUne++;
                partieDeux--;
            }
        }
        assertEquals(3,partieUne);
        assertEquals(0,partieDeux);
    }
}